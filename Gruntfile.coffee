'use strict'

module.exports = (grunt) ->

  grunt.initConfig

    exec:
      clean_generated_files:
        command: 'rm -rf lib/*'

    coffee:
      options:
        bare: true
        header: false
        sourceMap: false
      all:
        expand: true
        flatten: false
        cwd: 'src'
        src: ['**/*.coffee']
        dest: 'lib'
        ext: '.js'

    simplemocha:
      spec:
        options:
          reporter: 'list'
        src: 'spec/**/*_spec.coffee'
      specGrowl:
        options:
          reporter: 'min'
          growl: true
        src: 'spec/**/*_spec.coffee'

    watch:
      srcAndSpec:
        files: ['src/**/*.coffee', 'spec/**/*_spec.coffee']
        tasks: ['compile', 'simplemocha:specGrowl']

  grunt.loadNpmTasks 'grunt-exec'
  grunt.loadNpmTasks 'grunt-contrib-coffee'
  grunt.loadNpmTasks 'grunt-simple-mocha'
  grunt.loadNpmTasks 'grunt-contrib-watch'

  grunt.registerTask 'clean', ['exec:clean_generated_files']
  grunt.registerTask 'compile', ['coffee']
  grunt.registerTask 'test', ['compile', 'simplemocha:spec']
  grunt.registerTask 'dev', ['clean', 'compile', 'simplemocha:specGrowl', 'watch']
