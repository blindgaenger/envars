getModule = require '../lib/get'

module.exports = ->
  getModule.get.apply(getModule, arguments)

module.exports.env = getModule.currentEnv()
