path = require 'path'
fs = require 'fs'

module.exports =

  _findFiles: (localDir) ->
    files = []

    addFile = (dir) ->
      return unless dir?
      return unless fs.existsSync(dir)
      file = path.join(fs.realpathSync(dir), '.envars')
      return unless fs.existsSync(file)
      for f in files
        return if f == file
      files.push file

    addFile(localDir)
    addFile(process.cwd())
    addFile(process.env.HOME)

    files

  _readFile: (file) ->
    json = fs.readFileSync(file)
    JSON.parse(json)

  read: (localDir) ->
    files = @_findFiles(localDir)
    files.map(@_readFile)
