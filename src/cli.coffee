commander = require 'commander'
pkg = require '../package.json'
envars = require '../lib/envars'

command = new commander.Command() # own instance for testing
program = command
  .version(pkg.version)
  .usage('[options] <name>')
  .option('-e, --env [env]', "set the env [#{envars.defaultEnv}]")
  .option('-d, --default [value]', 'set the default value')
  .option('-S, --no-shell', 'ignore shell variables')
  .option('-R, --no-resolve', 'do not resolve references')
  .parse(process.argv)

name = program.args[0]
unless name?
  console.warn "no name was given"
  process.exit 1

options = {}
options.env = program.env if program.env?
options.default = program.default if program.default?
options.shell = program.shell if program.shell? and program.shell == false
options.resolve = program.resolve if program.resolve? and program.resolve == false

try
  vars = envars(name, options)
catch err
  console.warn "no value was found"
  process.exit 2

console.log vars
process.exit 0
