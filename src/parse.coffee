module.exports =

  SEPARATOR: '_'

  ParseError: class ParseError extends Error
    constructor: (@message='') ->
      @name = 'ParseError'

  _splitKeys: (keys) ->
    if keys.indexOf(@SEPARATOR) < 0
      [keys, null]
    else
      steps = keys.split(@SEPARATOR)
      key = steps.shift()
      path = steps.join(@SEPARATOR)
      [key, path]

  _isObject: (obj) ->
    obj == Object(obj)

  _assertObject: (obj, message) ->
    throw new @ParseError(message) unless @_isObject(obj)

  parse: (vars) ->
    for keys, values of vars
      [key, path] = @_splitKeys(keys)
      if path?
        delete vars[keys]

        subvars = vars[key] || {}
        @_assertObject(subvars, "could not expand key '#{keys}'")
        subvars[path] = values
        vars[key] = @parse(subvars)
      else
        if values == Object(values)
          vars[key] = @parse(values)

    vars
