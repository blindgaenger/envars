deepmerge = require 'deepmerge'
readModule = require '../lib/read'
parseModule = require '../lib/parse'

module.exports =

  DEFAULT_ENV: 'development'

  currentEnv: ->
    process.env.NODE_ENV or @DEFAULT_ENV

  _merge: (vars) ->
    result = {}
    for vals in vars.reverse()
      result = deepmerge(result, vals)
    result

  _getShellVar: (name) ->
    process.env[name] if name?

  _getEnvVars: (localDir) ->
    unparsedVars = readModule.read(localDir)
    parsedVars = unparsedVars.map (unparsed) -> parseModule.parse(unparsed)
    vars = @_merge(parsedVars)

  _getEnvVar: (env, name) ->
    return unless name?

    vars = @_getEnvVars()[env]
    return unless vars?

    current = vars
    for key in name.split(parseModule.SEPARATOR)
      current = current[key]
      return unless current?
    current

  _getVar: (name, options) ->
    if not options.shell? or options.shell == true
      value = @_getShellVar(name)
      return value if typeof value isnt 'undefined'

    env = options.env or @currentEnv()
    value = @_getEnvVar(env, name)
    return value if typeof value isnt 'undefined'

    value = options.default
    return value if typeof value isnt 'undefined'

    throw new Error('no value was found')

  _resolveVars: (value, options) ->
    return value unless typeof value == 'string'

    cache = {}
    value.replace /\$(\w+)/g, (match, name) =>
      cache[name] ||= @get(name, options)

  get: (name, options={}) ->
    value = @_getVar(name, options)

    if not options.resolve? or options.resolve == true
      value = @_resolveVars(value, options)

    value
