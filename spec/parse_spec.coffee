{sinon, sandbox, expect, proxyquire} = require './spec_helper'

parseModule = require '../lib/parse'

describe 'parse module', ->

  describe '#_splitKeys', ->
    it 'returns null for path for a normal key', ->
      expect(parseModule._splitKeys('foo')).to.eql(['foo', null])

    it 'splits the key and path', ->
      expect(parseModule._splitKeys('foo_bar_baz')).to.eql(['foo', 'bar_baz'])

  describe '#parse', ->
    it 'returns an object', ->
      actual =
        foo: 'bar'
      expected =
        foo: 'bar'

      expect(parseModule.parse(actual)).to.eql(expected)

    it 'expands keys', ->
      actual =
        'foo_bar': 'baz'
      expected =
        foo:
          bar: 'baz'

      expect(parseModule.parse(actual)).to.eql(expected)

    it 'expands deep keys', ->
      actual =
        'foo_bar_baz': 'foobarbaz'
      expected =
        foo:
          bar:
            baz: 'foobarbaz'

      expect(parseModule.parse(actual)).to.eql(expected)

    it 'expands deep subkeys', ->
      actual =
        foo:
          'bar_baz': 'foobarbaz'
      expected =
        foo:
          bar:
            baz: 'foobarbaz'

      expect(parseModule.parse(actual)).to.eql(expected)

    it 'expands multiple keys', ->
      actual =
        'foo_bar': 'foobar'
        'foo_baz': 'foobaz'
      expected =
        foo:
          bar: 'foobar'
          baz: 'foobaz'

      expect(parseModule.parse(actual)).to.eql(expected)

    it 'reuses existing keys', ->
      actual =
        foo:
          bar: 'foobar'
        'foo_baz': 'foobaz'
      expected =
        foo:
          bar: 'foobar'
          baz: 'foobaz'

      expect(parseModule.parse(actual)).to.eql(expected)

    it 'throws a error if existing keys are not objects', ->
      actual =
        foo: 'foo'
        'foo_baz': 'foobaz'

      expect(-> parseModule.parse(actual)).to.throw(parseModule.ParseError, "could not expand key 'foo_baz'")
