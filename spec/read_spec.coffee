{sinon, sandbox, expect, proxyquire} = require './spec_helper'

path = require 'path'
fs = require 'fs'
readModule = proxyquire '../lib/read', {path, fs}

describe 'read module', ->

  describe '#_findFiles', ->
    beforeEach ->
      @sinon = sandbox()
      @sinon.stub(process, 'cwd', -> '/cwd_dir')
      @sinon.stub(process, 'env', HOME: '/home_dir')

      @existsSync = @sinon.stub(fs, 'existsSync')
      @existsSync.returns(true)

    afterEach ->
      @sinon.restore()

    it 'returns .envars files from local, cwd and home', ->
      @sinon.stub(fs, 'realpathSync', (dir) -> "/real#{dir}")

      files = readModule._findFiles('/local_dir')

      expect(files).to.eql([
        '/real/local_dir/.envars'
        '/real/cwd_dir/.envars'
        '/real/home_dir/.envars'
      ])

    it 'returns existing files only', ->
      @sinon.stub(fs, 'realpathSync', (dir) -> "/real#{dir}")

      @existsSync.withArgs('/local_dir').returns(true)
      @existsSync.withArgs('/real/local_dir/.envars').returns(true)
      @existsSync.returns(false)

      files = readModule._findFiles('/local_dir')

      expect(files).to.eql([
        '/real/local_dir/.envars'
      ])

    it 'returns a found file only once', ->
      @sinon.stub(fs, 'realpathSync', (dir) -> '/one/dir/for/all')

      files = readModule._findFiles('/local_dir')

      expect(files).to.eql([
        '/one/dir/for/all/.envars'
      ])

  describe '#_readFile', ->
    beforeEach ->
      @sinon = sandbox()
      @readFileSync = @sinon.stub(fs, 'readFileSync')

    afterEach ->
      @sinon.restore()

    it 'returns the variables from the JSON file', ->
      @readFileSync.withArgs('foo.json').returns('{ "foo": "FOO" }')

      vars = readModule._readFile('foo.json')

      expect(vars).to.eql({foo: 'FOO'})

  describe '#read', ->
    beforeEach ->
      @sinon = sandbox()

    afterEach ->
      @sinon.verify()
      @sinon.restore()

    it 'reads the found files', ->
      files = ['foo.json', 'bar.json']
      fooVars = sinon.stub()
      barVars = sinon.stub()

      mock = @sinon.mock(readModule)
      mock.expects('_findFiles').withArgs('/local_dir').returns(files)
      mock.expects('_readFile').withArgs('foo.json').returns(fooVars)
      mock.expects('_readFile').withArgs('bar.json').returns(barVars)

      result = readModule.read('/local_dir')

      expect(result).to.eql([ fooVars, barVars ])
