{sinon, sandbox, expect, proxyquire} = require './spec_helper'

path = require 'path'
fs = require 'fs'
{exec} = require 'child_process'

TEST_VARS =
  development:
    foo: 'DEV_FOO'
    one: '$two'
    two: 'DEV_TWO'

  production:
    foo: 'PROD_FOO'

describe 'integration', ->
  before ->
    @tmpDir = path.join(__dirname, '..', 'tmp')
    @envarsFile = path.join(@tmpDir, '.envars')
    fs.unlinkSync(@envarsFile) if fs.existsSync(@envarsFile)
    fs.rmdirSync(@tmpDir) if fs.existsSync(@tmpDir)

    fs.mkdirSync(@tmpDir)
    json = JSON.stringify(TEST_VARS, null, 2)
    fs.writeFileSync(@envarsFile, json, 'utf8')

  after ->
    fs.unlinkSync(@envarsFile) if fs.existsSync(@envarsFile)
    fs.rmdirSync(@tmpDir) if fs.existsSync(@tmpDir)

  describe 'cli', ->
    it 'shows the help', (done) ->
      command = '../bin/envars --help'
      exec command, cwd: @tmpDir, (err, stdout, stderr) ->
        expect(err).to.not.exist
        expect(stdout).to.match(/Usage: envars/)
        expect(stderr).to.be.empty
        done()

    it 'shows an error for a missing name', (done) ->
      command = '../bin/envars'
      exec command, cwd: @tmpDir, (err, stdout, stderr) ->
        expect(err).to.exist
        expect(err.code).to.eql(1)
        expect(stdout).to.be.empty
        expect(stderr.trim()).to.eql('no name was given')
        done()

    it 'uses the shell variable', (done) ->
      command = 'foo=SHELL_FOO ../bin/envars foo'
      exec command, cwd: @tmpDir, (err, stdout, stderr) ->
        expect(err).to.not.exist
        expect(stdout.trim()).to.eql('SHELL_FOO')
        expect(stderr).to.be.empty
        done()

    it 'ignores the shell variable', (done) ->
      command = 'foo=SHELL_FOO ../bin/envars --no-shell foo'
      exec command, cwd: @tmpDir, (err, stdout, stderr) ->
        expect(err).to.not.exist
        expect(stdout.trim()).to.eql('DEV_FOO')
        expect(stderr).to.be.empty
        done()

    it 'gets an existing value', (done) ->
      command = '../bin/envars foo'
      exec command, cwd: @tmpDir, (err, stdout, stderr) ->
        expect(err).to.not.exist
        expect(stdout.trim()).to.eql('DEV_FOO')
        expect(stderr).to.be.empty
        done()

    it 'shows an error for a missing value', (done) ->
      command = '../bin/envars missing'
      exec command, cwd: @tmpDir, (err, stdout, stderr) ->
        expect(err).to.exist
        expect(err.code).to.eql(2)
        expect(stdout).to.be.empty
        expect(stderr.trim()).to.eql('no value was found')
        done()

    it 'uses the default for a missing value', (done) ->
      command = '../bin/envars --default DEFAULT missing'
      exec command, cwd: @tmpDir, (err, stdout, stderr) ->
        expect(err).to.not.exist
        expect(stdout.trim()).to.eql('DEFAULT')
        expect(stderr).to.be.empty
        done()

    it 'resolves a referenced value', (done) ->
      command = '../bin/envars one'
      exec command, cwd: @tmpDir, (err, stdout, stderr) ->
        expect(err).to.not.exist
        expect(stdout.trim()).to.eql('DEV_TWO')
        expect(stderr).to.be.empty
        done()

    it 'skips to resolve a referenced value', (done) ->
      command = '../bin/envars --no-resolve one'
      exec command, cwd: @tmpDir, (err, stdout, stderr) ->
        expect(err).to.not.exist
        expect(stdout.trim()).to.eql('$two')
        expect(stderr).to.be.empty
        done()

    it 'uses the provided environment', (done) ->
      command = '../bin/envars --env production foo'
      exec command, cwd: @tmpDir, (err, stdout, stderr) ->
        expect(err).to.not.exist
        expect(stdout.trim()).to.eql('PROD_FOO')
        expect(stderr).to.be.empty
        done()
