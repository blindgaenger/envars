{sinon, sandbox, expect, proxyquire} = require './spec_helper'

readModule = require '../lib/read'
parseModule = require '../lib/parse'
getModule = proxyquire '../lib/get', {
  '../lib/read': readModule
  '../lib/parse': parseModule
}

describe 'get module', ->

  describe '#currentEnv', ->
    it 'returns the process env variable', ->
      sandbox (sinon) ->
        sinon.stub(process, 'env', NODE_ENV: 'node_env')

        result = getModule.currentEnv()

        expect(result).to.eql('node_env')

    it 'defaults to development', ->
      sandbox (sinon) ->
        sinon.stub(process, 'env', NODE_ENV: undefined)

        result = getModule.currentEnv()

        expect(result).to.equal(getModule.DEFAULT_ENV)

  describe '#_merge', ->
    it 'merges the variables in the right order', ->
      unmergedVars = [
        { foo: "this", bar: "is"           }
        { foo: "FAIL",           baz: "it" }
      ]
      mergedVars = {foo: 'this', bar: 'is', baz: 'it'}

      result = getModule._merge(unmergedVars)

      expect(result).to.eql(mergedVars)

    it 'deep merges the variables', ->
      unmergedVars = [
        foo:
          bar: 'BAR'
      ,
        foo:
          baz: 'BAZ'
      ]
      mergedVars =
        foo:
          bar: 'BAR'
          baz: 'BAZ'

      result = getModule._merge(unmergedVars)

      expect(result).to.eql(mergedVars)

  describe '#_getShellVar', ->
    it 'returns undefined if the variable was not found', ->
      result = getModule._getShellVar('foo')

      expect(result).to.be.undefined

    it 'returns the value of a variable', ->
      sandbox (sinon) ->
        sinon.stub(process, 'env', foo: 'FOO')

        result = getModule._getShellVar('foo')

        expect(result).to.eql('FOO')

  describe '#_getEnvVars', ->
    beforeEach ->
      @sinon = sandbox()

    afterEach ->
      @sinon.verify()
      @sinon.restore()

    it 'parses and merges the vars from the files', ->
      dir = '/local'
      unparsed = @sinon.stub()
      parsed = @sinon.stub()
      vars = @sinon.stub()

      readMock = @sinon.mock(readModule)
      readMock.expects('read').withArgs(dir).returns([unparsed])

      parseMock = @sinon.mock(parseModule)
      parseMock.expects('parse').withArgs(unparsed).returns(parsed)

      getMock = @sinon.mock(getModule)
      getMock.expects('_merge').withArgs([parsed]).returns(vars)

      result = getModule._getEnvVars(dir)

      expect(result).to.eql(vars)

  describe '#_getEnvVar', ->
    beforeEach ->
      @sinon = sandbox()

    afterEach ->
      @sinon.verify()
      @sinon.restore()

    it 'returns undefined if the env was not found', ->
      @sinon.stub(getModule, '_getEnvVars', -> {})

      result = getModule._getEnvVar('dev', 'foo')

      expect(result).to.be.undefined

    it 'returns undefined if the variable was not found', ->
      @sinon.stub(getModule, '_getEnvVars', -> {
        dev: {}
      })

      result = getModule._getEnvVar('dev', 'foo')

      expect(result).to.be.undefined

    it 'returns the value of a variable', ->
      @sinon.stub(getModule, '_getEnvVars', -> {
        dev:
          foo: 'FOO'
      })

      result = getModule._getEnvVar('dev', 'foo')

      expect(result).to.eql('FOO')

    it 'returns the value of a deep variable', ->
      vars =
        dev:
          foo:
            bar: 'FOOBAR'
      @sinon.stub(getModule, '_getEnvVars', -> vars)

      result = getModule._getEnvVar('dev', 'foo_bar')

      expect(result).to.eql('FOOBAR')

  describe '#_getVar', ->
    beforeEach ->
      @sinon = sandbox()
      @getModuleMock = @sinon.mock(getModule)

    afterEach ->
      @sinon.verify()
      @sinon.restore()

    context 'no option', ->
      it 'returns the matching shell var', ->
        @getModuleMock.expects('_getShellVar').withExactArgs('foo').returns('FOO')

        result = getModule._getVar('foo', {})

        expect(result).to.eql('FOO')

      it 'uses the current env', ->
        currentEnv = @sinon.stub(getModule, 'currentEnv', -> 'dev')
        getVar = @sinon.stub(getModule, '_getEnvVar')

        try
          getModule._getVar('foo', {})
        catch
          # don't care

        expect(getVar).to.have.been.calledWith('dev', 'foo')

      it 'returns the value of a found variable', ->
        getVar = @sinon.stub(getModule, '_getEnvVar')
        getVar.withArgs(sinon.match.defined, 'foo').returns('FOO')

        result = getModule._getVar('foo', {})

        expect(result).to.eql('FOO')

      it 'returns throws an error for missing variable', ->
        getVar = @sinon.stub(getModule, '_getEnvVar')
        getVar.withArgs(sinon.match.defined, 'foo').returns(undefined)

        result = -> getModule._getVar('foo', {})

        expect(result).to.throw('no value was found')

    context 'default option', ->
      it 'returns the matching shell var', ->
        @getModuleMock.expects('_getShellVar').withExactArgs('foo').returns('FOO')

        result = getModule._getVar('foo', {})

        expect(result).to.eql('FOO')

      it 'returns the variable value', ->
        getVar = @sinon.stub(getModule, '_getEnvVar')
        getVar.withArgs(sinon.match.defined, 'foo').returns('FOO')

        result = getModule._getVar('foo', {default: 'BAR'})

        expect(result).to.eql('FOO')

      it 'returns the default value', ->
        getVar = @sinon.stub(getModule, '_getEnvVar')
        getVar.withArgs(sinon.match.defined, 'foo').returns(undefined)

        result = getModule._getVar('foo', {default: 'BAR'})

        expect(result).to.eql('BAR')

      it 'returns the default value (when is null)', ->
        getVar = @sinon.stub(getModule, '_getEnvVar')
        getVar.withArgs(sinon.match.defined, 'foo').returns(undefined)

        result = getModule._getVar('foo', {default: null})

        expect(result).to.eql(null)

      it 'returns throws an error for missing variable', ->
        getVar = @sinon.stub(getModule, '_getEnvVar')
        getVar.withArgs(sinon.match.defined, 'foo').returns(undefined)

        result = -> getModule._getVar('foo', {default: undefined})

        expect(result).to.throw('no value was found')

    context 'env option', ->
      it 'returns the matching shell var', ->
        @getModuleMock.expects('_getShellVar').withExactArgs('foo').returns('FOO')

        result = getModule._getVar('foo', {})

        expect(result).to.eql('FOO')

      it 'uses the passed env', ->
        currentEnv = @sinon.stub(getModule, 'currentEnv', -> 'dev')
        getVar = @sinon.stub(getModule, '_getEnvVar')

        try
          getModule._getVar('foo', {env: 'prod'})
        catch
          # don't care

        expect(getVar).to.have.been.calledWith('prod', 'foo')

    context 'shell option', ->
      it 'returns the matching shell var', ->
        @getModuleMock.expects('_getShellVar').withExactArgs('foo').returns('FOO')

        result = getModule._getVar('foo', {shell: true})

        expect(result).to.eql('FOO')

      it 'skips the shell var', ->
        getShellVar = @sinon.stub(getModule, '_getShellVar')

        try
          getModule._getVar('foo', {shell: false})
        catch
          # don't care

        expect(getShellVar).to.have.not.been.called

  describe '#_resolveVars', ->
    beforeEach ->
      @sinon = sandbox()
      @getModuleMock = @sinon.mock(getModule)

    afterEach ->
      @sinon.verify()
      @sinon.restore()

    it 'passes null', ->
      value = null

      result = getModule._resolveVars(value)

      expect(result).to.eql(value)

    it 'passes non strings', ->
      value = {some: 'thing'}

      result = getModule._resolveVars(value)

      expect(result).to.eql(value)

    it 'passes strings without variables', ->
      value = 'VALUE'

      result = getModule._resolveVars(value)

      expect(result).to.eql(value)

    it 'passes thrown errors', ->
      getError = new Error('get')
      @getModuleMock.expects('get').throws(getError).once()

      try
        getModule._resolveVars('$argh')
      catch err
        expect(err).to.eql(getError)

    it 'resolves variables (with same options for #get) ', ->
      value = '$foo/$bar'
      options = 'OPTIONS'

      @getModuleMock.expects('get').withExactArgs('foo', options).returns('FOO')
      @getModuleMock.expects('get').withExactArgs('bar', options).returns('BAR')

      result = getModule._resolveVars(value, options)

      expect(result).to.eql('FOO/BAR')

    it 'resolves multiple variable occurences (resolves values only once)', ->
      value = '$foo/$foo'
      options = 'OPTIONS'

      @getModuleMock.expects('get').withExactArgs('foo', options).returns('FOO').once()

      result = getModule._resolveVars(value, options)

      expect(result).to.eql('FOO/FOO')

  describe '#get', ->
    beforeEach ->
      @sinon = sandbox()
      @getModuleMock = @sinon.mock(getModule)

    afterEach ->
      @sinon.verify()
      @sinon.restore()

    it 'gets and resolves variables with the same options', ->
      name = 'NAME'
      options = 'OPTIONS'
      unresolvedValue = '$REF'
      value = 'VALUE'

      @getModuleMock.expects('_getVar').withExactArgs(name, options).returns(unresolvedValue)
      @getModuleMock.expects('_resolveVars').withExactArgs(unresolvedValue, options).returns(value)

      result = getModule.get(name, options)

      expect(result).to.eql(value)

    context 'resolve option', ->
      it 'resolves the vars', ->
        options = {resolve: true}
        unresolvedValue = '$REF'
        value = 'VALUE'

        @getModuleMock.expects('_getVar').returns(unresolvedValue)
        @getModuleMock.expects('_resolveVars').withExactArgs(unresolvedValue, options).returns(value)

        result = getModule.get('DONTCARE', options)

        expect(result).to.eql(value)

      it 'skips to resolve the vars', ->
        options = {resolve: false}
        unresolvedValue = '$VALUE'

        @getModuleMock.expects('_getVar').returns(unresolvedValue)
        @getModuleMock.expects('_resolveVars').never()

        result = getModule.get('DONTCARE', options)

        expect(result).to.eql(unresolvedValue)
