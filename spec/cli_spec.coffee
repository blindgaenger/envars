{sinon, sandbox, expect, proxyquire} = require './spec_helper'

stubCommand = (options, callback) ->
  sandbox (sinon, envars, exec) ->
    sinon.stub(process, 'argv', ['node', 'envars'].concat(options.args))
    exitError = new Error('EXIT')
    exitStub = sinon.stub(process, 'exit').throws(exitError)

    logStub = sinon.stub(console, 'log') if options.stdout?
    warnStub = sinon.stub(console, 'warn') if options.stderr?

    envars = sinon.stub()
    exec = -> proxyquire '../lib/cli', {'../lib/envars': envars}

    try
      callback(sinon, envars, exec)
    catch e
      expect(e).to.equal(exitError, 'unknown error was thrown')

    expect(logStub).to.have.been.calledWith(options.stdout) if options.stdout?
    expect(warnStub).to.have.been.calledWith(options.stderr) if options.stderr?
    expect(exitStub).to.have.been.calledOnce
    expect(exitStub).to.have.been.calledWith(options.exitcode)

describe 'cli', ->

  context 'no options', ->
    it 'fails if no name was given', ->
      stubCommand
        args: []
        stderr: 'no name was given'
        exitcode: 1
        (sinon, envars, exec) ->
          exec()

          expect(envars).to.have.not.been.called

    it 'fails if no value was found', ->
      stubCommand
        args: ['foo']
        stderr: 'no value was found'
        exitcode: 2
        (sinon, envars, exec) ->
          envars.withArgs('foo', {}).throws(new Error('no value was found'))

          exec()

          expect(envars).to.have.been.calledOnce
          expect(envars).to.have.been.calledWith('foo', {})

    it 'succeeds if a value was found', ->
      stubCommand
        args: ['foo']
        stdout: 'bar'
        exitcode: 0
        (sinon, envars, exec) ->
          envars.withArgs('foo', {}).returns('bar')

          exec()

          expect(envars).to.have.been.calledOnce
          expect(envars).to.have.been.calledWith('foo', {})

  context 'default option', ->
    it 'succeeds if a default value was given', ->
      stubCommand
        args: ['--default', 'bar', 'foo']
        stdout: 'bar'
        exitcode: 0
        (sinon, envars, exec) ->
          envars.withArgs('foo', {default: 'bar'}).returns('bar')

          exec()

          expect(envars).to.have.been.calledOnce
          expect(envars).to.have.been.calledWith('foo', {default: 'bar'})

  context 'env option', ->
    it 'uses the passed environment', ->
      stubCommand
        args: ['--env', 'dev', 'foo']
        stdout: 'bar'
        exitcode: 0
        (sinon, envars, exec) ->
          envars.withArgs('foo', {env: 'dev'}).returns('bar')

          exec()

          expect(envars).to.have.been.calledOnce
          expect(envars).to.have.been.calledWith('foo', {env: 'dev'})

  context 'no-shell option', ->
    it 'ignores shell variables', ->
      stubCommand
        args: ['--no-shell', 'foo']
        stdout: 'bar'
        exitcode: 0
        (sinon, envars, exec) ->
          envars.withArgs('foo', {shell: false}).returns('bar')

          exec()

          expect(envars).to.have.been.calledOnce
          expect(envars).to.have.been.calledWith('foo', {shell: false})

  context 'no-resolve option', ->
    it 'does not resolve references', ->
      stubCommand
        args: ['--no-resolve', 'foo']
        stdout: '$foo'
        exitcode: 0
        (sinon, envars, exec) ->
          envars.withArgs('foo', {resolve: false}).returns('$foo')

          exec()

          expect(envars).to.have.been.calledOnce
          expect(envars).to.have.been.calledWith('foo', {resolve: false})
