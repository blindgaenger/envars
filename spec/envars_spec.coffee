{sinon, sandbox, expect, proxyquire} = require './spec_helper'

getModule = require '../lib/get'
sinon.stub(getModule, 'currentEnv', -> 'CURRENT_ENV')
envars = proxyquire '../lib/envars', {'../lib/get': getModule}

describe 'envars', ->

  it 'exports #get as a function', ->
    sandbox (sinon) ->
      get = sinon.stub(getModule, 'get')
      get.withArgs('NAME').returns('VALUE')

      result = envars('NAME')

      expect(get).to.be.calledWith('NAME')
      expect(get).to.be.calledOn(getModule)
      expect(result).to.eql('VALUE')

  it 'exports the current #env', ->
    expect(envars.env).to.equal('CURRENT_ENV')
