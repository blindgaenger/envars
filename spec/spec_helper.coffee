sinon = require 'sinon'
chai = require 'chai'
sinonChai = require 'sinon-chai'
chai.use sinonChai
expect = chai.expect
proxyquire = require 'proxyquire'

sandbox = (fn) ->
  box = sinon.sandbox.create()
  if arguments.length == 0
    box
  else
    try
      fn(box)
    finally
      box.restore()

module.exports = {sinon, sandbox, expect, proxyquire}
