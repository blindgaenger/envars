var fs, path;

path = require('path');

fs = require('fs');

module.exports = {
  _findFiles: function(localDir) {
    var addFile, files;
    files = [];
    addFile = function(dir) {
      var f, file, _i, _len;
      if (dir == null) {
        return;
      }
      if (!fs.existsSync(dir)) {
        return;
      }
      file = path.join(fs.realpathSync(dir), '.envars');
      if (!fs.existsSync(file)) {
        return;
      }
      for (_i = 0, _len = files.length; _i < _len; _i++) {
        f = files[_i];
        if (f === file) {
          return;
        }
      }
      return files.push(file);
    };
    addFile(localDir);
    addFile(process.cwd());
    addFile(process.env.HOME);
    return files;
  },
  _readFile: function(file) {
    var json;
    json = fs.readFileSync(file);
    return JSON.parse(json);
  },
  read: function(localDir) {
    var files;
    files = this._findFiles(localDir);
    return files.map(this._readFile);
  }
};
