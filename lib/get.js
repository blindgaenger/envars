var deepmerge, parseModule, readModule;

deepmerge = require('deepmerge');

readModule = require('../lib/read');

parseModule = require('../lib/parse');

module.exports = {
  DEFAULT_ENV: 'development',
  currentEnv: function() {
    return process.env.NODE_ENV || this.DEFAULT_ENV;
  },
  _merge: function(vars) {
    var result, vals, _i, _len, _ref;
    result = {};
    _ref = vars.reverse();
    for (_i = 0, _len = _ref.length; _i < _len; _i++) {
      vals = _ref[_i];
      result = deepmerge(result, vals);
    }
    return result;
  },
  _getShellVar: function(name) {
    if (name != null) {
      return process.env[name];
    }
  },
  _getEnvVars: function(localDir) {
    var parsedVars, unparsedVars, vars;
    unparsedVars = readModule.read(localDir);
    parsedVars = unparsedVars.map(function(unparsed) {
      return parseModule.parse(unparsed);
    });
    return vars = this._merge(parsedVars);
  },
  _getEnvVar: function(env, name) {
    var current, key, vars, _i, _len, _ref;
    if (name == null) {
      return;
    }
    vars = this._getEnvVars()[env];
    if (vars == null) {
      return;
    }
    current = vars;
    _ref = name.split(parseModule.SEPARATOR);
    for (_i = 0, _len = _ref.length; _i < _len; _i++) {
      key = _ref[_i];
      current = current[key];
      if (current == null) {
        return;
      }
    }
    return current;
  },
  _getVar: function(name, options) {
    var env, value;
    if ((options.shell == null) || options.shell === true) {
      value = this._getShellVar(name);
      if (typeof value !== 'undefined') {
        return value;
      }
    }
    env = options.env || this.currentEnv();
    value = this._getEnvVar(env, name);
    if (typeof value !== 'undefined') {
      return value;
    }
    value = options["default"];
    if (typeof value !== 'undefined') {
      return value;
    }
    throw new Error('no value was found');
  },
  _resolveVars: function(value, options) {
    var cache,
      _this = this;
    if (typeof value !== 'string') {
      return value;
    }
    cache = {};
    return value.replace(/\$(\w+)/g, function(match, name) {
      return cache[name] || (cache[name] = _this.get(name, options));
    });
  },
  get: function(name, options) {
    var value;
    if (options == null) {
      options = {};
    }
    value = this._getVar(name, options);
    if ((options.resolve == null) || options.resolve === true) {
      value = this._resolveVars(value, options);
    }
    return value;
  }
};
