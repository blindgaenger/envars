var getModule;

getModule = require('../lib/get');

module.exports = function() {
  return getModule.get.apply(getModule, arguments);
};

module.exports.env = getModule.currentEnv();
