var command, commander, envars, err, name, options, pkg, program, vars;

commander = require('commander');

pkg = require('../package.json');

envars = require('../lib/envars');

command = new commander.Command();

program = command.version(pkg.version).usage('[options] <name>').option('-e, --env [env]', "set the env [" + envars.defaultEnv + "]").option('-d, --default [value]', 'set the default value').option('-S, --no-shell', 'ignore shell variables').option('-R, --no-resolve', 'do not resolve references').parse(process.argv);

name = program.args[0];

if (name == null) {
  console.warn("no name was given");
  process.exit(1);
}

options = {};

if (program.env != null) {
  options.env = program.env;
}

if (program["default"] != null) {
  options["default"] = program["default"];
}

if ((program.shell != null) && program.shell === false) {
  options.shell = program.shell;
}

if ((program.resolve != null) && program.resolve === false) {
  options.resolve = program.resolve;
}

try {
  vars = envars(name, options);
} catch (_error) {
  err = _error;
  console.warn("no value was found");
  process.exit(2);
}

console.log(vars);

process.exit(0);
