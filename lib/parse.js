var ParseError,
  __hasProp = {}.hasOwnProperty,
  __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

module.exports = {
  SEPARATOR: '_',
  ParseError: ParseError = (function(_super) {
    __extends(ParseError, _super);

    function ParseError(message) {
      this.message = message != null ? message : '';
      this.name = 'ParseError';
    }

    return ParseError;

  })(Error),
  _splitKeys: function(keys) {
    var key, path, steps;
    if (keys.indexOf(this.SEPARATOR) < 0) {
      return [keys, null];
    } else {
      steps = keys.split(this.SEPARATOR);
      key = steps.shift();
      path = steps.join(this.SEPARATOR);
      return [key, path];
    }
  },
  _isObject: function(obj) {
    return obj === Object(obj);
  },
  _assertObject: function(obj, message) {
    if (!this._isObject(obj)) {
      throw new this.ParseError(message);
    }
  },
  parse: function(vars) {
    var key, keys, path, subvars, values, _ref;
    for (keys in vars) {
      values = vars[keys];
      _ref = this._splitKeys(keys), key = _ref[0], path = _ref[1];
      if (path != null) {
        delete vars[keys];
        subvars = vars[key] || {};
        this._assertObject(subvars, "could not expand key '" + keys + "'");
        subvars[path] = values;
        vars[key] = this.parse(subvars);
      } else {
        if (values === Object(values)) {
          vars[key] = this.parse(values);
        }
      }
    }
    return vars;
  }
};
