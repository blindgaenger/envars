# envars

Your environment, your variables!


## Code

Load the envars of the current environment (`development` by default).

    var envars = require('envars')();
    var foo = envars.foo;
    # => DEV_FOO

Or loading the variables of another environment.

    var envars = require('envars')('production');
    var foo = envars.foo;
    # => PROD_FOO

See the section below on how to set the environment from the outside.


## CLI

There is also a shell tool to get the vars.

    $ envars foo
    DEV_FOO

    $ envars --env production foo
    PROD_FOO

It's nice to get and store the variables for other usage

    $ foo=$(envars foo)
    $ echo $foo
    DEV_FOO


## Define your variables

Put all needed variables into an `.envars` file in the current working directory. It's a simple
JSON file like this:

    {
      "development": {
        "foo": "DEV_FOO"
      },
      "production": {
        "foo": "PROD_FOO"
      }
    }

You can also put a `.envars` in your `$HOME` dir to define global variables. These will be merged with
the `.envars` loaded from your current working directory. A good place to store your github, twitter,
whatever credentials.


## Environments

The default environment is `development`, but it can easily be overridden. Either set `NODE_ENV`,
use the `--env` option for the CLI, or as an argument in your code.

    $ NODE_ENV=production node app.js
    $ NODE_ENV=production envars foo

    $ envars --env production foo

    var envars = require('envars')('production');

That's it!
