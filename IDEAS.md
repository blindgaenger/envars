# Definitions

variable
: a pair of name and value

name
: a single or multiple keys

key
: an identifier without a separator

value
: null or a string, which can contain references

reference
: a reference to a key

# Example .envars file

    development:
      foo: DEV_FOO

    production:
      foo: PROD_FOO


# Get variables

## default behaviour

$ envars foo
DEV_FOO

## explicit environment

$ envars -e production foo
PROD_FOO

$ NODE_ENV=production envars foo
PROD_FOO

## overwrite with shell variable

$ foo=SHELL_FOO
$ envars foo
SHELL_FOO


# Reference variables

# Syntax

$SHELL_VAR
@env
@env.bar
bar

## resolve from shell variable

    development:
      foo: "{{ $shell_var }}"

$ shell_var=SHELL_VAR
$ envars foo
SHELL_VAR

## reference another variable (same env)

    development:
      foo: "{{ bar }}"
      bar: "DEV_BAR"

$ envars foo
DEV_BAR

## reference another variable (different env)

    development:
      foo: "{{ /production/foo }}"

    production:
      foo: PROD_FOO

$ envars foo
PROD_FOO

## resolve current env

    development:
      foo: "{{ @env }}"

$ envars foo
development


# Tasks

## TODO

- use shell variable to skip integration tests
- add test helpers in Makefile
- check for cyclic variable references
- drop cli_spec in favor of integration_spec
- rename integration_spec to acceptance_spec
- mock filesystem in acceptance_spec
  for extracting deployment vars, which should not be overwritten
- update readme
- set envars via cli
- manpage in package.json
- other output format [json, export shell]
- better syntax (see examples above)
- envars object

    envars.get('foo')
    => found value

    envars('foo')
    => delegates to #get

    envars = require('envars')
    whinyEnvars = envars({required: true})
    => new envars instance with given defaults

    envars.defaults
    => current defaults

    envars.env
    => current env

- multiple values

    envars(['username', 'password'])
    => ['foobar', 'secret']

    envars(['existing', 'missing'])
    => Error: key 'missing' is missing

    envars(['existing', 'missing'], default: 'ARGH')
    => ['foobar', 'ARGH']

    envars(['existing', 'missing'], default: ['YAY', 'ARGH'])
    => ['foobar', 'ARGH']

- _defaults in .envars file
- _options for env_var
  "env_var": "$NODE_ENV"
  "env_var": "$RACK_ENV"
  "env_var": "$RAILS_ENV"
- support for coffee script .envars files
- resolve other envars (not shell only)
  "apiToken": "f1o2o3"
  "apiUrl": "http://service.ws/$apiToken/more 

## DOIT


## DONE

- rename "replace" to "resolve"
- option to not resolve vars
- use "name" instead of "key" or "variable"
- resolve from shell variable
- option to skip shell vars
- keys are required by default

  envars('existing')
  => 'YAY'
  envars('existing', default: 'ARGH')
  => 'YAY'
  envars('missing')
  => Error: key 'missing' is missing
  envars('missing', default: 'ARGH')
  => 'ARGH'

- overwrite with shell variable
- require '../lib/parse'
- cli_spec
- option default value to cli and require
- var separator . => :
- option environment
- use envars.default_env in cli
- Makefile => grunt.js for watch

